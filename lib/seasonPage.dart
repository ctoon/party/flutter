import 'package:ctoon/watchPage.dart';
import 'package:flutter/material.dart';
import 'package:ctoon/CTOONdata.dart';

class SeasonPage extends StatefulWidget {
  SeasonPage({Key key, this.show, this.season}) : super(key: key);
  final Shows show;
  final Seasons season;

  @override
  _SeasonPageState createState() => _SeasonPageState();
}

class _SeasonPageState extends State<SeasonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.show.name),
      ),
      body: SingleChildScrollView(
        child: _buildPage(),
      ),
    );
  }

  Widget _buildPage() {
    return Container (
        padding: EdgeInsets.symmetric(vertical: 20.0),
        width: double.infinity,
        child: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Container(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  widget.season.title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 21.0
                  ),
                ),
              ),
            ),
            _buildList(),
          ],
        )
    );
  }

  Widget _buildList() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.symmetric(vertical: 12.0),
      itemCount: widget.season.episodes.length,
      itemBuilder: (context, i) {
        final item = widget.season.episodes[i];
        return ListTile(
            leading: ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: 78,
                minHeight: 44,
                maxWidth: 78,
                maxHeight: 44,
              ),
              child: FadeInImage(
                placeholder: AssetImage('assets/icon/ct-icon-letters-inverse.png'),
                image: NetworkImage(item.thumbnail),
                alignment: Alignment.center,
                fit: BoxFit.contain,
              ),
            ),
            title: Text(item.title),
            subtitle: Text(
              item.sxe.short
            ),
            onTap: () {
              _episodePressed(
                  item
              );
            }
        );
      },
    );
  }

  void _episodePressed(Episodes episode) async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WatchPage(
          showName: widget.show.name,
          showShort: widget.show.shortName,
          episode: episode
        )
      ),
    );
  }
}