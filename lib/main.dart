import 'dart:convert';
import 'package:ctoon/CTOONdata.dart';
import 'package:ctoon/showPage.dart';
import 'package:ctoon/watchPage.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ctoon/globals.dart' as g;

void main() => runApp(CTOON());

class CTOON extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CTOON [ALPHA]',
      theme: ThemeData(
        primaryColor: Color.fromRGBO(78, 67, 118, 1.0),
        accentColor: Color.fromRGBO(43, 88, 118, 1.0),
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<CTOONData> loadedData;

  Future<CTOONData> fetchData() async {
    var response = await http.get(g.baseAPI + '/all');

    if (response.statusCode == 200) {
      return CTOONData.fromJson(json.decode(response.body));
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Something went wrong...'),
            content: Text(
              json.decode(response.body)['message']
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
                child: Text('Quit'),
              ),
            ],
          );
        }
      );

      return CTOONData();
    }
  }

  Future<void> _refreshData() async {
    setState(() {
      loadedData = fetchData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadedData = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CTOON [ALPHA]'),
      ),
      body: RefreshIndicator(
        onRefresh: _refreshData,
        child: SingleChildScrollView(
          child: _buildPage(),
        ),
      ),
    );
  }

  Widget _buildPage() {
    return Container (
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            child: Container(
              padding: EdgeInsets.only(left: 12.0),
              child: Text(
                'Latest:',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 21.0
                ),
              ),
            ),
          ),
          _buildLatestList(),
          SizedBox(
            width: double.infinity,
            child: Container(
              padding: EdgeInsets.only(left: 12.0),
              child: Text(
                'Shows:',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 21.0
                ),
              ),
            ),
          ),
          _buildShowList(),
        ],
      )
    );
  }

  Widget _buildLatestList() {
    return FutureBuilder<CTOONData>(
        future: loadedData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.latest != null && snapshot.data.latest.length == 0) {
              return Center(
                  child: Text('Loading failed.')
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                primary: false,
                padding: EdgeInsets.symmetric(vertical: 12.0),
                itemCount: snapshot.data.latest.length,
                itemBuilder: (context, i) {
                  final item = snapshot.data.latest[i];

                  return ListTile(
                      leading: ConstrainedBox(
                        constraints: BoxConstraints(
                          minWidth: 78,
                          minHeight: 44,
                          maxWidth: 78,
                          maxHeight: 44,
                        ),
                        child: FadeInImage(
                          placeholder: AssetImage('assets/icon/ct-icon-letters-inverse.png'),
                          image: NetworkImage(item.episode.thumbnail),
                          alignment: Alignment.center,
                          fit: BoxFit.contain,
                        ),
                      ),
                      title: Text(item.episode.title),
                      subtitle: Text(item.episode.sxe.short + ' - ' + item.show.name),
                      onTap: () {
                        _latestPressed(
                            item
                        );
                      }
                  );
                },
              );
            }
          } else if (snapshot.hasError) {
            return Center(
                child: Text('Error: ${snapshot.error}')
            );
          }

          // Default, while waiting
          return Center(
              child: CircularProgressIndicator()
          );
        }
    );
  }

  void _latestPressed(Latest item) async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WatchPage(
          showName: item.show.name,
          showShort: item.show.shortName,
          episode: item.episode
        )
      ),
    );
  }

  Widget _buildShowList() {
    return FutureBuilder<CTOONData>(
        future: loadedData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.latest != null && snapshot.data.latest.length == 0) {
              return Center(
                  child: Text('Loading failed.')
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                primary: false,
                padding: EdgeInsets.symmetric(vertical: 12.0),
                itemCount: snapshot.data.shows.length,
                itemBuilder: (context, i) {
                  final item = snapshot.data.shows[i];

                  return ListTile(
                      title: Text(item.name),
                      subtitle: Text(item.rating),
                      onTap: () {
                        _showPressed(
                            item
                        );
                      }
                  );
                },
              );
            }
          } else if (snapshot.hasError) {
            return Center(
                child: Text('Error: ${snapshot.error}')
            );
          }

          // Default, while waiting
          return Center(
              child: CircularProgressIndicator()
          );
        }
    );
  }

  void _showPressed(Shows item) async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ShowPage(
          show: item
        )
      ),
    );
  }
}
