// This file is literally me dumping the whole /api/1.1/all into
// https://javiercbk.github.io/json_to_dart/
// TODO: Fuse show and shows in one and clean some stuff

class CTOONData {
  List<Shows> shows;
  List<Latest> latest;

  CTOONData({this.shows, this.latest});

  CTOONData.fromJson(Map<String, dynamic> json) {
    if (json['shows'] != null) {
      shows = List<Shows>();
      json['shows'].forEach((v) {
        shows.add(Shows.fromJson(v));
      });
    }
    if (json['latest'] != null) {
      latest = List<Latest>();
      json['latest'].forEach((v) {
        latest.add(Latest.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.shows != null) {
      data['shows'] = this.shows.map((v) => v.toJson()).toList();
    }
    if (this.latest != null) {
      data['latest'] = this.latest.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Shows {
  int id;
  String shortName;
  String name;
  String rating;
  String banner;
  String cover;
  String description;
  String descriptionSource;
  Links links;
  List<Seasons> seasons;

  Shows(
      {this.id,
        this.shortName,
        this.name,
        this.rating,
        this.banner,
        this.cover,
        this.description,
        this.descriptionSource,
        this.links,
        this.seasons});

  Shows.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shortName = json['short_name'];
    name = json['name'];
    rating = json['rating'];
    banner = json['banner'];
    cover = json['cover'];
    description = json['description'];
    descriptionSource = json['description_source'];
    links = json['links'] != null ? Links.fromJson(json['links']) : null;
    if (json['seasons'] != null) {
      seasons = List<Seasons>();
      json['seasons'].forEach((v) {
        seasons.add(Seasons.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['short_name'] = this.shortName;
    data['name'] = this.name;
    data['rating'] = this.rating;
    data['banner'] = this.banner;
    data['cover'] = this.cover;
    data['description'] = this.description;
    data['description_source'] = this.descriptionSource;
    if (this.links != null) {
      data['links'] = this.links.toJson();
    }
    if (this.seasons != null) {
      data['seasons'] = this.seasons.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Links {
  String official;
  String fanNews;
  String imdb;
  String itunes;
  String amazon;
  String googlePlay;

  Links(
      {this.official,
        this.fanNews,
        this.imdb,
        this.itunes,
        this.amazon,
        this.googlePlay});

  Links.fromJson(Map<String, dynamic> json) {
    official = json['official'];
    fanNews = json['fan_news'];
    imdb = json['imdb'];
    itunes = json['itunes'];
    amazon = json['amazon'];
    googlePlay = json['google_play'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['official'] = this.official;
    data['fan_news'] = this.fanNews;
    data['imdb'] = this.imdb;
    data['itunes'] = this.itunes;
    data['amazon'] = this.amazon;
    data['google_play'] = this.googlePlay;
    return data;
  }
}

class Seasons {
  String title;
  List<Episodes> episodes;

  Seasons({this.title, this.episodes});

  Seasons.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    if (json['episodes'] != null) {
      episodes = List<Episodes>();
      json['episodes'].forEach((v) {
        episodes.add(Episodes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['title'] = this.title;
    if (this.episodes != null) {
      data['episodes'] = this.episodes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Episodes {
  int id;
  String title;
  String season;
  String episode;
  Sxe sxe;
  String publishedDate;
  String thumbnail;

  Episodes(
      {this.id,
        this.title,
        this.season,
        this.episode,
        this.sxe,
        this.publishedDate,
        this.thumbnail});

  Episodes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    season = json['season'];
    episode = json['episode'];
    sxe = json['sxe'] != null ? Sxe.fromJson(json['sxe']) : null;
    publishedDate = json['published_date'];
    thumbnail = json['thumbnail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['season'] = this.season;
    data['episode'] = this.episode;
    if (this.sxe != null) {
      data['sxe'] = this.sxe.toJson();
    }
    data['published_date'] = this.publishedDate;
    data['thumbnail'] = this.thumbnail;
    return data;
  }
}

class Sxe {
  String short;
  String long;
  String group;

  Sxe({this.short, this.long, this.group});

  Sxe.fromJson(Map<String, dynamic> json) {
    short = json['short'];
    long = json['long'];
    group = json['group'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['short'] = this.short;
    data['long'] = this.long;
    data['group'] = this.group;
    return data;
  }
}

class Latest {
  Episodes episode;
  Show show;

  Latest({this.episode, this.show});

  Latest.fromJson(Map<String, dynamic> json) {
    episode =
    json['episode'] != null ? Episodes.fromJson(json['episode']) : null;
    show = json['show'] != null ? Show.fromJson(json['show']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.episode != null) {
      data['episode'] = this.episode.toJson();
    }
    if (this.show != null) {
      data['show'] = this.show.toJson();
    }
    return data;
  }
}

class Show {
  int id;
  String shortName;
  String name;
  String rating;
  String banner;
  String cover;
  String description;
  String descriptionSource;
  Links links;

  Show(
      {this.id,
        this.shortName,
        this.name,
        this.rating,
        this.banner,
        this.cover,
        this.description,
        this.descriptionSource,
        this.links});

  Show.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shortName = json['short_name'];
    name = json['name'];
    rating = json['rating'];
    banner = json['banner'];
    cover = json['cover'];
    description = json['description'];
    descriptionSource = json['description_source'];
    links = json['links'] != null ? Links.fromJson(json['links']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['short_name'] = this.shortName;
    data['name'] = this.name;
    data['rating'] = this.rating;
    data['banner'] = this.banner;
    data['cover'] = this.cover;
    data['description'] = this.description;
    data['description_source'] = this.descriptionSource;
    if (this.links != null) {
      data['links'] = this.links.toJson();
    }
    return data;
  }
}

class Episode {
  int id;
  String title;
  String season;
  String episode;
  Sxe sxe;
  String publishedDate;
  String thumbnail;
  Files files;

  Episode(
      {this.id,
        this.title,
        this.season,
        this.episode,
        this.sxe,
        this.publishedDate,
        this.thumbnail,
        this.files});

  Episode.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    season = json['season'];
    episode = json['episode'];
    sxe = json['sxe'] != null ? Sxe.fromJson(json['sxe']) : null;
    publishedDate = json['published_date'];
    thumbnail = json['thumbnail'];
    files = json['files'] != null ? Files.fromJson(json['files']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['season'] = this.season;
    data['episode'] = this.episode;
    if (this.sxe != null) {
      data['sxe'] = this.sxe.toJson();
    }
    data['published_date'] = this.publishedDate;
    data['thumbnail'] = this.thumbnail;
    if (this.files != null) {
      data['files'] = this.files.toJson();
    }
    return data;
  }
}
class Files {
  Webm webm;
  Mp4 mp4;
  String hls;
  List<Null> subtitles;
  String seekThumbnails;

  Files({this.webm, this.mp4, this.hls, this.subtitles, this.seekThumbnails});

  Files.fromJson(Map<String, dynamic> json) {
    webm = json['webm'] != null ? Webm.fromJson(json['webm']) : null;
    mp4 = json['mp4'] != null ? Mp4.fromJson(json['mp4']) : null;
    hls = json['hls'];
    seekThumbnails = json['seek_thumbnails'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.webm != null) {
      data['webm'] = this.webm.toJson();
    }
    if (this.mp4 != null) {
      data['mp4'] = this.mp4.toJson();
    }
    data['hls'] = this.hls;
    if (this.subtitles != null) {
      data['subtitles'] = this.subtitles;
    }
    data['seek_thumbnails'] = this.seekThumbnails;
    return data;
  }
}

class Webm {
  String s240;
  String s480;
  String s720;
  String s1080;

  Webm({this.s240, this.s480, this.s720, this.s1080});

  Webm.fromJson(Map<String, dynamic> json) {
    s240 = json['240'];
    s480 = json['480'];
    s720 = json['720'];
    s1080 = json['1080'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['240'] = this.s240;
    data['480'] = this.s480;
    data['720'] = this.s720;
    data['1080'] = this.s1080;
    return data;
  }
}

class Mp4 {
  String s480;

  Mp4({this.s480});

  Mp4.fromJson(Map<String, dynamic> json) {
    s480 = json['480'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['480'] = this.s480;
    return data;
  }
}