import 'package:ctoon/seasonPage.dart';
import 'package:flutter/material.dart';
import 'package:ctoon/CTOONdata.dart';

class ShowPage extends StatefulWidget {
  ShowPage({Key key, this.show}) : super(key: key);
  final Shows show;

  @override
  _ShowPageState createState() => _ShowPageState();
}

class _ShowPageState extends State<ShowPage> {
  ImageProvider banner;

  @override
  void initState() {
    super.initState();
    banner = NetworkImage(widget.show.banner);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(banner, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.show.name),
      ),
      body: SingleChildScrollView(
        child: _buildPage(),
      ),
    );
  }

  Widget _buildPage() {
    return Container (
        width: double.infinity,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 150.0,
              width: double.infinity,
              child: Stack(
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                  Center(
                    child: FadeInImage(
                      placeholder: AssetImage('assets/empty.png'),
                      image: banner,
                      alignment: Alignment.center,
                      fit: BoxFit.fitHeight,
                      height: 150.0,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Container(
                padding: EdgeInsets.only(left: 12.0, top: 12.0),
                child: Text(widget.show.description),
              ),
            ),
            _buildList(),
          ],
        )
    );
  }

  Widget _buildList() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.symmetric(vertical: 12.0),
      itemCount: widget.show.seasons.length,
      itemBuilder: (context, i) {
        final item = widget.show.seasons[i];
        return ListTile(
            title: Text(item.title),
            subtitle: Text(
              item.episodes.length.toString() + ' element' +
              (item.episodes.length > 1 ? 's' : '')
            ),
            onTap: () {
              _seasonPressed(
                  item
              );
            }
        );
      },
    );
  }

  void _seasonPressed(Seasons season) async {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SeasonPage(
            show: widget.show,
            season: season
          )
      ),
    );
  }
}