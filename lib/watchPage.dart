import 'dart:ui';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:ctoon/CTOONdata.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';
import 'package:http/http.dart' as http;
import 'package:ctoon/globals.dart' as g;

class WatchPage extends StatefulWidget {
  WatchPage({Key key, this.showName, this.showShort, this.episode}) : super(key: key);
  final String showName;
  final String showShort;
  final Episodes episode;

  @override
  _WatchPageState createState() => _WatchPageState();
}

class _WatchPageState extends State<WatchPage> {
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;

  Future<void> fetchData() async {
    var response = await http.get(g.baseAPI + '/' + widget.showShort + '/' + widget.episode.id.toString());
    if (response.statusCode == 200) {
      Episode res = Episode.fromJson(json.decode(response.body)['episode']);
      String vidUrl = res.files.webm.s720 != null ? res.files.webm.s720 : res.files.webm.s480;
      _videoPlayerController = VideoPlayerController.network(vidUrl);
      initPlayer();
    }
  }

  Future<void> initPlayer() async {
    await _videoPlayerController.initialize();
    double asp = _videoPlayerController.value.aspectRatio;
    if (asp == 1.7791666666666666) { // It's actually 4:3
      asp = 4 / 3;
    }

    setState(() {
      _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController,
        aspectRatio: asp,
        autoPlay: true,
      );
    });
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text(widget.showName),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _chewieController != null ? Chewie(
              controller: _chewieController,
            ) : AspectRatio(
              aspectRatio: 16 / 9,
              child: Center(
                child: CircularProgressIndicator()
              ),
            ),
            Container(
              padding: EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.episode.title,
                    style: TextStyle(
                      fontSize: 21.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(widget.episode.sxe.long),
                ],
              ),
            )
          ]
        ),
      ),
    );
  }
}